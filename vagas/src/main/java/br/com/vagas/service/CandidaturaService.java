package br.com.vagas.service;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vagas.model.Candidatura;

public interface CandidaturaService extends JpaRepository<Candidatura, Long> {
	 

}
