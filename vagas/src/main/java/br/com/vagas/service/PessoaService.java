package br.com.vagas.service;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vagas.model.Pessoa;

public interface PessoaService extends JpaRepository<Pessoa, Long> {
	 

}
