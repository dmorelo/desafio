package br.com.vagas.service;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vagas.model.Vaga;

public interface VagaService extends JpaRepository<Vaga, Long> {
	 

}
