package br.com.vagas.constant;

public class Number {

	public static final Integer NUMBER_0 = 0;
	public static final Integer NUMBER_5 = 5;
	public static final Integer NUMBER_10 = 10;
	public static final Integer NUMBER_15 = 15;
	public static final Integer NUMBER_20 = 20;
	public static final Integer NUMBER_25 = 25;
	public static final Integer NUMBER_50 = 50;
	public static final Integer NUMBER_75 = 75;
	public static final Integer NUMBER_100 = 100;
}
