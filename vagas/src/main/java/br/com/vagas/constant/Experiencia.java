package br.com.vagas.constant;

public enum Experiencia {
	
	ESTAGIARIO(1, "estagiário"),
	JUNIOR(2, "júnior"),
	PLENO(3, "pleno"),
	SENIOR(4, "sênior"),
	ESPECIALISTA(5, "especialista");
	
	public Integer id;
	public String descricao;
	
	private Experiencia(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public Experiencia getExperiencia(Integer id) {
		for (Experiencia experiencia : Experiencia.values()) {
			if (experiencia.getId() == id) {
				return experiencia;
			}
		}
		
		return null;
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}


}
