package br.com.vagas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Candidatura implements Serializable {
	 
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@OneToOne
	private Pessoa pessoa;
	
	@OneToOne
	private Vaga vaga;
	
	private Integer score;
	
	
	public Candidatura() {
		
	}


	public Candidatura(Long id, Pessoa pessoa, Vaga vaga, Integer score) {
		this.id = id;
		this.pessoa = pessoa;
		this.vaga = vaga;
		this.score = score;
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the pessoa
	 */
	public Pessoa getPessoa() {
		return pessoa;
	}


	/**
	 * @param pessoa the pessoa to set
	 */
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}


	/**
	 * @return the vaga
	 */
	public Vaga getVaga() {
		return vaga;
	}


	/**
	 * @param vaga the vaga to set
	 */
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}


	/**
	 * @return the score
	 */
	public Integer getScore() {
		return score;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(Integer score) {
		this.score = score;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
