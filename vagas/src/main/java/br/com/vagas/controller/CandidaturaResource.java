package br.com.vagas.controller;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vagas.constant.Number;
import br.com.vagas.model.Candidatura;
import br.com.vagas.route.Aresta;
import br.com.vagas.route.Grafo;
import br.com.vagas.route.Vertice;
import br.com.vagas.service.CandidaturaService;

@RestController
@RequestMapping("/candidaturas")
public class CandidaturaResource implements ApplicationContextAware {

	@Autowired
	private CandidaturaService candidaturaService;
	
	@Autowired
	private ApplicationContext applicationContext;


	@GetMapping("/{id}")
	public Candidatura buscar(@PathVariable Long id) {
		return this.candidaturaService.findOne(id);
	}
	
	@GetMapping()
	public List<Candidatura> pesquisar() {
		return this.candidaturaService.findAll();
	}

	@GetMapping("/ranking")
	public List<Candidatura> pesquisarRanking() {
		return this.candidaturaService.findAll(new Sort(Sort.Direction.ASC, "score"));
	}

	@PostMapping
	public Candidatura salvar(@RequestBody Candidatura candidatura) {
		PessoaResource cliente = (PessoaResource) this.applicationContext.getBean("pessoaResource");
		candidatura.setPessoa(cliente.buscar(candidatura.getPessoa().getId()));
	
		VagaResource vagaResource = (VagaResource) this.applicationContext.getBean("vagaResource");
		candidatura.setVaga(vagaResource.buscar(candidatura.getVaga().getId()));
		
		candidatura.setScore(generateScore(candidatura));
		
		return this.candidaturaService.save(candidatura);
	}
	
	@PostMapping("/list")
	public List<Candidatura> salvar(@RequestBody List<Candidatura> listaCandidaturas) {
		
		for (Candidatura candidatura : listaCandidaturas) {
			PessoaResource cliente = (PessoaResource) this.applicationContext.getBean("pessoaResource");
			candidatura.setPessoa(cliente.buscar(candidatura.getPessoa().getId()));
		
			VagaResource vagaResource = (VagaResource) this.applicationContext.getBean("vagaResource");
			candidatura.setVaga(vagaResource.buscar(candidatura.getVaga().getId()));
			
			candidatura.setScore(generateScore(candidatura));
			
			this.candidaturaService.save(candidatura);
		}
		
		return listaCandidaturas;
	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		this.candidaturaService.delete(id);
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		 this.applicationContext = applicationContext;
	}
	
	public int generateScore(Candidatura candidatura) {
		Integer nivelCandidato = candidatura.getPessoa().getNivel();
		Integer nivelVaga = candidatura.getVaga().getNivel();
		
		double nivel = (Number.NUMBER_100 - Number.NUMBER_25) * (nivelVaga - nivelCandidato);
		
		Grafo inicial = getGrafoInicial();
		Grafo resultado = new Grafo();
		
		Vertice verticeCandidato = inicial.acharVertice(candidatura.getPessoa().getLocalizacao());
		Vertice verticeVaga = inicial.acharVertice(candidatura.getVaga().getLocalizacao());
		resultado.setVertices(inicial.encontrarMenorCaminhoDijkstra(verticeCandidato, verticeVaga));
		
		int pesoDistancia = 0;
		for (Aresta aresta : resultado.getArestas()) {
			pesoDistancia += aresta.getPeso();
		}
		
		
		Integer distancia = 0;
		if (pesoDistancia <= Number.NUMBER_5) {
			distancia = Number.NUMBER_100;
			
		} else if (pesoDistancia > Number.NUMBER_5 && pesoDistancia <= Number.NUMBER_10) {
			distancia = Number.NUMBER_75;
			
		} else if (pesoDistancia > Number.NUMBER_10 && pesoDistancia <= Number.NUMBER_15) {
			distancia = Number.NUMBER_50;
			
		}  else if (pesoDistancia > Number.NUMBER_15 && pesoDistancia <= Number.NUMBER_20) {
			distancia = Number.NUMBER_25;
			
		}  else if (pesoDistancia > Number.NUMBER_20) {
			distancia = Number.NUMBER_0;
		}
		
		
		return (int) (nivel + distancia) / 2;
	}
	
	public Grafo getGrafoInicial() {
		
		Grafo inicial = new Grafo();
		inicial.addAresta(5, "A", "B");
		inicial.addAresta(7, "B", "C");
		inicial.addAresta(3, "B", "D");
		inicial.addAresta(4, "C", "E");
		inicial.addAresta(10, "D", "E");
		inicial.addAresta(8, "D", "F");
		
		return inicial;
	}

}
