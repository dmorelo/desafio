package br.com.vagas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vagas.model.Vaga;
import br.com.vagas.service.VagaService;

@RestController
@RequestMapping("/vagas")
public class VagaResource {

	@Autowired
	private VagaService vagaService;

	@GetMapping("/{id}")
	public Vaga buscar(@PathVariable Long id) {
		return this.vagaService.findOne(id);
	}

	@GetMapping
	public List<Vaga> pesquisar() {
		return this.vagaService.findAll();
	}

	@PostMapping
	public Vaga salvar(@RequestBody Vaga vaga) {
		return this.vagaService.save(vaga);
	}
	
	@PostMapping("/list")
	public List<Vaga> salvar(@RequestBody List<Vaga> listaVagas) {
		
		for (Vaga vaga : listaVagas) {
			this.vagaService.save(vaga);
		}
		
		return listaVagas;
	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		this.vagaService.delete(id);
	}

}
