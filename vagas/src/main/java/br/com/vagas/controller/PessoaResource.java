package br.com.vagas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vagas.model.Pessoa;
import br.com.vagas.service.PessoaService;

@RestController
@RequestMapping("/pessoas")
public class PessoaResource {

	@Autowired
	private PessoaService pessoaService;

	@GetMapping("/{id}")
	public Pessoa buscar(@PathVariable Long id) {
		return this.pessoaService.findOne(id);
	}

	@GetMapping
	public List<Pessoa> pesquisar() {
		return this.pessoaService.findAll();
	}

	@PostMapping
	public Pessoa salvar(@RequestBody Pessoa pessoa) {
		return this.pessoaService.save(pessoa);
	}
	
	@PostMapping("/list")
	public List<Pessoa> salvar(@RequestBody List<Pessoa> listaPessoas) {
		
		for (Pessoa pessoa : listaPessoas) {
			this.pessoaService.save(pessoa);
		}
		
		return listaPessoas;
	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		this.pessoaService.delete(id);
	}

}
